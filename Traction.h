/// @file    Traction.h
/// @created 2/01/2016
/// @author  bdeeming

#ifndef TRACTION_H_
#define TRACTION_H_

#include <Arduino.h>

class Traction {
public:
    Traction(int speedPin_, int directionPinA_, int directionPinB_)
        : speedPin(speedPin_),
          directionPinA(directionPinA_),
          directionPinB(directionPinB_) {
        pinMode(speedPin, OUTPUT);
        pinMode(directionPinA, OUTPUT);
        pinMode(directionPinB, OUTPUT);

        Brake();
    }

    void Brake() {
        analogWrite(speedPin, 0);

        // Break = 0b00
        digitalWrite(directionPinA, LOW);
        digitalWrite(directionPinB, LOW);
    }

    void Drive(int speed) {
        // Set direction
        if (speed < 0) {
            // Reverse = 0b10
            digitalWrite(directionPinA, LOW);
            digitalWrite(directionPinB, HIGH);

            // Make speed positive (saves a call to abs() later)
            speed = -speed;
        } else {
            // Forward = 0b01
            digitalWrite(directionPinA, HIGH);
            digitalWrite(directionPinB, LOW);
        }

        // Limit top speed to prevent motor burn out
        if (speed > MAX_MOTOR_SPEED) {
            speed = MAX_MOTOR_SPEED;
        }

        // Set speed via PWM
        analogWrite(speedPin, speed);
    }

private:
    /// Rated max is 3V (we're running a 5V system)
    static constexpr int MAX_MOTOR_SPEED = 255 * 3 / 5;

    int speedPin;
    int directionPinA;
    int directionPinB;
};

#endif /* TRACTION_H_ */
