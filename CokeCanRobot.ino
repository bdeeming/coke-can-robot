/// @file    CokeCanRobot.ino
/// @created 2/01/2016
/// @author  bdeeming

#include <stdint.h>

#include <HardwareSerial.h>
#include <NewPing.h>

#include "Traction.h"

/** This class allows the normal use of constructors to initialize class instances
 * instead of requiring a global + begin() method.
 */
class Main {
    friend void setup();
    friend void loop();

    static inline Main& GetInstance() {
        static Main main;
        return main;
    }

    static constexpr uint8_t LED_PIN = UINT8_C(13);

    static constexpr uint8_t LEFT_EDGE_PIN = UINT8_C(2);
    static constexpr uint8_t RIGHT_EDGE_PIN = UINT8_C(6);

    /// The period between sonar ping's (ms)
    static constexpr auto PING_INTERVAL = 10ul;

    /// The maximum amount of time to search for a target by turning on the spot (approx 225deg)
    static constexpr auto MAX_SEARCH_TIME = 450ul;

    /// The maximum distance up to which a target is considered 'near'
    static constexpr auto TARGET_NEAR_DISTANCE = 30;

    static constexpr int EDGE_TURN_SPEED = 80;
    static constexpr auto SEARCH_TURN_SPEED = 20;

    Traction tractionLeft{/*speed=*/10, /*dir=*/4, /*dir=*/5};
    Traction tractionRight{/*speed=*/9, /*dir=*/7, /*dir=*/8};

    NewPing sonar{/*trig=*/11, /*echo=*/12, /*maxdist_cm=*/70};

    /// Time at which we should trigger the next ping
    unsigned long nextPingTime = 0ul;

    /// Time-stamp when we last started a search
    unsigned long searchStartTime;

    /// True when target is actively being pinged
    bool isTargetLock = false;

    enum class TargetType {
        NONE,
        NEAR,
        FAR
    } targetType = TargetType::NONE;

    enum class State {
        EDGE_TURN,
        SEARCHING_STRAIGHT,
        SEARCHING_TURN,
        ACQUIRED
    } currentState = State::SEARCHING_TURN;

    enum class TurnDirection {
        LEFT,
        RIGHT
    } turnDirection = TurnDirection::LEFT;

    Main() {
        Serial.begin(115200);

        pinMode(LED_PIN, OUTPUT);
        digitalWrite(LED_PIN, LOW);

        pinMode(LEFT_EDGE_PIN, INPUT_PULLUP);
        pinMode(RIGHT_EDGE_PIN, INPUT_PULLUP);

        searchStartTime = millis();
    }

    void Loop() {
        auto currentTime = millis();

        // Check if we're due to start a new sonar ping
        if (currentTime >= nextPingTime) {
            nextPingTime += PING_INTERVAL;
            sonar.ping_timer(SonarCb);
        }

        // Edge sensors are high when OFF the table
        auto isEdgeLeft = digitalRead(LEFT_EDGE_PIN);
        auto isEdgeRight = digitalRead(RIGHT_EDGE_PIN);

        // State transitions
        switch (currentState) {
            case State::SEARCHING_TURN:
                // Stay in SEARCHING_TURN if edge is found

                if (!isEdgeLeft && !isEdgeRight) {
                    // Check if search timer is expired
                    if ((currentTime - searchStartTime) > MAX_SEARCH_TIME) {
                        currentState = State::SEARCHING_STRAIGHT;
                    }

                    if (isTargetLock) {
                        currentState = State::ACQUIRED;
                    }
                }
                break;

            case State::SEARCHING_STRAIGHT:
                // Avoid edges
                if (isEdgeLeft) {
                    currentState = State::EDGE_TURN;
                    turnDirection = TurnDirection::RIGHT;
                } else if (isEdgeRight) {
                    currentState = State::EDGE_TURN;
                    turnDirection = TurnDirection::LEFT;
                } else if (isTargetLock) {
                    currentState = State::ACQUIRED;
                }
                break;

            case State::EDGE_TURN:
                if (!isEdgeLeft && !isEdgeRight) {
                    currentState = State::SEARCHING_TURN;
                    searchStartTime = currentTime;
                }
                break;

            case State::ACQUIRED:
                // Avoid edges
                if (isEdgeLeft) {
                    currentState = State::EDGE_TURN;
                    turnDirection = TurnDirection::RIGHT;
                } else if (isEdgeRight) {
                    currentState = State::EDGE_TURN;
                    turnDirection = TurnDirection::LEFT;
                } else if (!isTargetLock) {
                    currentState = (targetType == TargetType::NEAR) ? State::SEARCHING_STRAIGHT
                                                                      :
                                                                      State::SEARCHING_TURN;
                }
                break;
        }

        // State machine outputs
        switch (currentState) {
            case State::ACQUIRED: // Fall-through
            case State::SEARCHING_STRAIGHT:
                tractionLeft.Drive(EDGE_TURN_SPEED);
                tractionRight.Drive(EDGE_TURN_SPEED);
                break;

            case State::EDGE_TURN:
                Turn(EDGE_TURN_SPEED);
                break;

            case State::SEARCHING_TURN:
                Turn(SEARCH_TURN_SPEED);
                break;
        }
    }

    void Turn(int speed) {
        if (turnDirection == TurnDirection::LEFT) {
            tractionLeft.Drive(-speed);
            tractionRight.Drive(speed);
        } else {
            tractionLeft.Drive(speed);
            tractionRight.Drive(-speed);
        }
    }

    // NewPing (Timer2) interrupt calls this function every 24us so we can check the ping status
    static void SonarCb() {
        Main::GetInstance().SonarCheck();
    }

    void SonarCheck() {
        // Check to see if the ping was received
        if (sonar.check_timer()) {
            isTargetLock = sonar.ping_result != NO_ECHO;
            digitalWrite(LED_PIN, isTargetLock);

            // Update the target type any time we get a reading on it
            if (isTargetLock) {
                auto pingDistance = sonar.ping_result / US_ROUNDTRIP_CM;
                targetType = (pingDistance < TARGET_NEAR_DISTANCE) ? TargetType::NEAR : TargetType::FAR;
            }
        }
    }

};

void setup() {
    Main::GetInstance();
}

void loop() {
    Main::GetInstance().Loop();
}

