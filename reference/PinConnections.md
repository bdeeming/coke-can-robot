|| Driver Board || Arduino  || Description      ||
|  PB1 (Pin 15) |  P10 (PWM)|  Speed L          |
|  PD6 (Pin 12) |  P4       |  Direction L      |
|  PD7 (Pin 13) |  P5       |  Direction L      |
|  PB6 (Pin 9)  |  P7       |  Direction R      |
|  PB7 (Pin 10) |  P8       |  Direction R      |
|  PB2 (Pin 16) |  P9 (PWM) |  Speed R          |
|  (Pin 22)     |  GND      |  Common Ground    |
|               |  P11      |  Sonar trigger    |
|               |  P12      |  Sonar echo       |
|               |  P13      |  Target acquired LED |
|               |  P2       |  Left edge sensor |
|               |  P6       |  Right edge sensor |

